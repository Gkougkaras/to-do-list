//Check off the to-do when clicking on them
$("ul").on("click","li", function(){
    $(this).toggleClass("completed");
});

//click on X to delete a to-do
$("ul").on("click","span", function(event){
    $(this).parent().fadeOut("1000s",function(){
        $(this).remove();
    });
    event.stopPropagation();
});

//Add a listener to the input
var placeholder = " ";
$("input[type='text']").keypress(function(event){
if(event.which === 13){
    var todoText = $(this).val();
    $(this).val(" ");
    $("ul").append("<li><span><i class='fa fa-trash'></i></span> " + todoText + "</li>");
}
});

$(".fa-pencil-alt").click(function(){
    $("input[type='text']").fadeToggle();
});